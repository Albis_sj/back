const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql2');
// const Connection = require('mysql2/typings/mysql/lib/Connection');
const multer = require('multer');
const app = express();


const storage = multer.diskStorage({
        destination: '../Frontend/src/assets/img/perfil',
        filename: function(req, file, cb){
            cb(null, 'AQUI VA EL NOMBRE' + '.' + file.mimetype.split('/')[1])
        }
})

const upload = multer({storage: storage})
app.use(cors());
app.post('/', upload.single('file'), )


app.use(cors());
app.use(bodyparser.json());



// database conecction
const db = mysql.createConnection({
  host:'localhost',
  user:'root',
  password:'',
  database:'camara_db',
  port:3306
});

 // check database Connection

db.connect(err=>{
  if (err){console.log(err, 'dberr ALBA');}
  console.log('database connected ...');
})




//=====================================================
//====================CATEGORIA========================
//=====================================================

// get all data
app.get('/categoria', (req, res)=> {
    // console.log('get categoria');
    let qr = `SELECT * FROM Categoria`;

    db.query(qr,(err,result)=> {
        if(err)
        {
            console.log(err, 'errs');
        }

        if(result.length>0)
        {
            res.send({
                message:'all categoria data',
                data:result
            });
        }
    });
});

// get single data
app.get('/categoria/:id',(req, res)=>{
        // console.log('get single data');
        // console.log(req.params.id, 'getid==>');
        let gID = req.params.id;

        let qr = `SELECT * FROM Categoria WHERE id_categoria = ${gID}`;

        db.query(qr,(err,result)=>{
            if(err) { console.log(err); }

            if(result.length>0)
            {
                  res.send({
                      message:'get single data',
                      data:result
                  });
            }
            else
            {
                  res.send({
                      message:'data not found'
                  })
            }
        })

})

// create data
app.post('/categoria',(req,res)=>{
        // console.log('post data')
        console.log(req.body, 'create data');

        let categoria = req.body.categoria;

        let qr = `INSERT INTO Categoria(categoria)
VALUES ('${categoria}')`
    console.log(qr, 'qr');
        db.query(qr,(err, result)=>{

            if(err){console.log(err);}
            // console.log(result,'result')
            res.send({
              message: 'data inserted',

            });
        });
});

//update single data
app.put('/categoria/:id',(req,res)=>{
        // console.log('update data');
        console.log(req.body, 'update data');

        let gID = req.params.id;
        let categoria = req.body.categoria;

        let qr = `UPDATE Categoria SET categoria = '${categoria}'
        WHERE id_categoria = ${gID}`;

        db.query(qr, (err, result)=> {
          
                if(err) {console.log(err);}

                res.send ({
                    message: 'data update'
                })
        })

})

//delete data
app.delete('/categoria/:id', (req,res)=>{
          let gID = req.params.id;

          let qr = `DELETE FROM Categoria WHERE id_categoria = '${gID}' `;
          db.query(qr, (err, result)=>{

                  if(err){console.log(err);}

                  res.send({
                    message: 'data deleted'
                  })
          })
})



//=====================================================
//=====================LIBROS===========================
//=====================================================

// get all data
app.get('/libros', (req, res)=> {
    console.log('get Libros');
    let qr = `SELECT * FROM Libros`;

    db.query(qr,(err,result)=> {
        if(err)
        {
            console.log(err, 'errs');
        }
        if(result.length>0)
        {
            res.send({
                message:'all libro data',
                data:result
            });
        }
    });
});

// get single data
app.get('/libros/:id',(req, res)=>{
        // console.log('get single data');
        // console.log(req.params.id, 'getid==>');
        let gID = req.params.id;

        let qr = `SELECT * FROM Libros WHERE id_libro = ${gID}`;

        db.query(qr,(err,result)=>{
            if(err) { console.log(err); }

            if(result.length>0)
            {
                  res.send({
                      message:'get single data',
                      data:result
                  });
            }
            else
            {
                  res.send({
                      message:'data not found'
                  })
            }
        })

})

// create data
app.post('/libros',(req,res)=>{
        // console.log('post data editorial')
        console.log(req.body, 'create data');

        let idioma = req.body.idioma;
        let titulo = req.body.titulo;
        let subtitulo = req.body.subtitulo;
        let autor_nombre = req.body.autor_nombre;
        let autor_apellido = req.body.autor_apellido;
        let descripcion = req.body.descripcion;
        let precio = req.body.precio;
        let formato = req.body.formato;
        let fecha = req.body.fecha;
        let imagen = req.body.imagen;
        let id_categoria = req.body.id_categoria;
        let id_editorial = req.body.id_editorial;
        console.log(id_categoria);

        let qr = `INSERT INTO Libros (idioma, titulo, subtitulo, autor_nombre, 
            autor_apellido, descripcion, precio, formato, fecha, imagen, id_categoria, id_editorial)
VALUES ('${idioma}', '${titulo}', '${subtitulo}', '${autor_nombre}', '${autor_apellido}', '${descripcion}', '${precio}', '${formato}', '${fecha}', '${imagen}',  '${id_categoria}',  '${id_editorial}')`
    console.log(qr, 'qr');
        db.query(qr,(err, result)=>{

            if(err){console.log(err);}
            // console.log(result,'result')
            res.send({
              message: 'data inserted',
            });
        });
});

//update single data
app.put('/libro/:id',(req,res)=>{
        // console.log('update data');
        console.log(req.body, 'update data');

        let gID = req.params.id;
        let nombre = req.body.nombre;
        let telefono1 = req.body.telefono1;
        let telefono2 = req.body.telefono2;
        let celular1 = req.body.celular1;
        let celular2 = req.body.celular2;
        let direccion = req.body.direccion;
        let correo1 = req.body.correo1;
        let correo2 = req.body.correo2;
        let pagina = req.body.pagina;

        let qr = `UPDATE Editorial SET nombre = '${nombre}', telefono1 = '${telefono1}', telefono2 = '${telefono2}', celular1 = '${celular1}', celular2 = '${celular2}', direccion = '${direccion}', correo1 = '${correo1}', correo2 = '${correo2}', pagina = '${pagina}'
        WHERE id_editorial = ${gID}`;

        db.query(qr, (err, result)=> {
          
                if(err) {console.log(err);}

                res.send ({
                    message: 'data update'
                })
        })

})

//delete data
app.delete('/libro/:id', (req,res)=>{
          let gID = req.params.id;

          let qr = `DELETE FROM Editorial WHERE id_editorial = '${gID}' `;
          db.query(qr, (err, result)=>{

                  if(err){console.log(err);}

                  res.send({
                    message: 'data deleted'
                  })
          })
})

//=====================================================
//=================== Vendedor ==================
//=====================================================

// get all data
app.get('/vendedor', (req, res)=> {
    console.log('get vendedor');
    let qr = `SELECT * FROM Vendedor`;

    db.query(qr,(err,result)=> {
        if(err)
        {
            console.log(err, 'errs');
        }
        if(result.length>0)
        {
            res.send({
                message:'all vendedor data',
                data:result
            });
        }
    });
});


// get single data
app.get('/vendedor/:id',(req, res)=>{
        console.log('get single data');
        // console.log(req.params.id, 'getid==>');
        let gID = req.params.id;

        console.log(gID);
        
        let qr = `SELECT * FROM Vendedor WHERE id_vendedor = ${gID}`;

        db.query(qr,(err,result)=>{
            if(err) { console.log(err); }

            if(result.length>0)
            {
                console.log('zzzzzzz',result.length, 'xxxxxxxx');
                  res.send({
                      message:'get single data',
                      data:result
                  });
            }
            else
            {
                  res.send({
                      message:'data not found'
                  })
            }
        })

})

//get PARAMS CORREO CONTRASENIA
const { response,request } = express

const usuarioGet = (req = request, res = response) => {

    const query = req.query;
    // const { correo1 } = req.query
    res.json({
        msg: 'get Api controlador',
        query
    });
};

// create data
app.post('/vendedor',(req,res)=>{
        // console.log('post data vendedor')
        console.log(req.body, 'create data');

        let nombre = req.body.nombre;
        let categoria = req.body.categoria;
        let correo1 = req.body.correo1;
        let contrasenia = req.body.contrasenia;
        let telefono1 = req.body.telefono1;
        let telefono2 = req.body.telefono2;
        let celular1 = req.body.celular1;
        let celular2 = req.body.celular2;
        let correo2 = req.body.correo2;
        let direccion = req.body.direccion;
        let pagina = req.body.pagina;

        let qr = `INSERT INTO Vendedor (nombre, categoria, correo1, contrasenia, telefono1, telefono2, celular1, celular2, correo2, direccion, pagina)
VALUES ('${nombre}', '${categoria}', '${correo1}', '${contrasenia}', '${telefono1}', '${telefono2}', '${celular1}', '${celular2}', '${correo2}', '${direccion}', '${pagina}')`
    console.log(qr, 'qr');
        db.query(qr,(err, result)=>{

            if(err){console.log(err);}
            // console.log(result,'result')
            res.send({
              message: 'data inserted',
            });
        });
});


//update single data
app.put('/vendedor/:id',(req,res)=>{
        // console.log('update data');
        console.log(req.body, 'update data');

        let gID = req.params.id;
        let nombre = req.body.nombre;
        let categoria = req.body.categoria;
        let correo1 = req.body.correo1;
        let contrasenia = req.body.contrasenia;
        let telefono1 = req.body.telefono1;
        let telefono2 = req.body.telefono2;
        let celular1 = req.body.celular1;
        let celular2 = req.body.celular2;
        let correo2 = req.body.correo2;
        let direccion = req.body.direccion;
        let pagina = req.body.pagina;

        let qr = `UPDATE Vendedor SET nombre = '${nombre}', categoria = '${categoria}', correo1 = '${correo1}', contrasenia = '${contrasenia}', telefono1 = '${telefono1}', telefono2 = '${telefono2}', celular1 = '${celular1}', celular2 = '${celular2}', correo2 = '${correo2}', direccion = '${direccion}', pagina = '${pagina}'
        WHERE id_vendedor = ${gID}`;

        db.query(qr, (err, result)=> {
          
                if(err) {console.log(err);}

                res.send ({
                    message: 'data update'
                })
        })

})


//delete data
app.delete('/vendedor/:id', (req,res)=>{
          let gID = req.params.id;

          let qr = `DELETE FROM Vendedor WHERE id_vendedor = '${gID}' `;
          db.query(qr, (err, result)=>{

                  if(err){console.log(err);}

                  res.send({
                    message: 'data deleted'
                  })
          })
})






//=====================================================
//=====================IMAGEN==========================
//=====================================================



// create data
app.post('/imagen',(req,res)=>{
    // console.log('post data editorial')
    console.log(req.body, 'create data');

//     let qr = `INSERT INTO Editorial(nombre, telefono1, telefono2, celular1, 
//         celular2, direccion, correo1, correo2, pagina)
// VALUES ('${nombre}', '${telefono1}', '${telefono2}', '${celular1}', '${celular2}', '${direccion}', '${correo1}', '${correo2}', '${pagina}')`
// console.log(qr, 'qr');
    db.query(qr,(err, result)=>{

        if(err){console.log(err);}
        // console.log(result,'result')
        res.send({
          message: 'It worked!',
        });
    });
});


app.listen(3000,()=>{
  console.log('server running ...');
})